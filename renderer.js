/* Protocol:
ALL_HELLO count => broadcast your messages count, if others have more messages, they broadcast their ALL_HELLO
ALL_MSG id text => broadcast new message
GET id => request for message with id
MSG id text => response message with id
*/

// imports:
const fs = require('fs');
const timers = require('timers')
const dgram = require('dgram');

// globals:
const ADDRESS = getMyIP('192.168.0.')[0];
const PORT = 45678;
console.log(`ADDRESS: ${ADDRESS}`);
const BROADCAST_ADDR = broadcastAddr(ADDRESS);
console.log(`BROADCAST_ADDR: ${BROADCAST_ADDR}`);

const STORAGE_FILE = 'storage.txt';
var MESSAGES = [];

var TIMER = null;

// socket setup
const server = dgram.createSocket('udp4');

server.on('error', (err) => {
  console.log(`error:\n${err.stack}`);
  server.close();
});

server.on('message', (msg, rinfo) => {
  console.log(`From ${rinfo.address}:${rinfo.port}: ${msg}`);
  if (rinfo.address == ADDRESS) {
    console.log("It is from me, ignoring it.");
    return;
  }
  msg = msg.toString();
  var handler = msg.startsWith('ALL_HELLO') ? handleAllHello :
                msg.startsWith('ALL_MSG')   ? handleAllMsg :
                msg.startsWith('GET')       ? handleGet :
                msg.startsWith('MSG')       ? handleMsg :
                                              handleIgnore;
  handler(msg, rinfo);
});

server.on('listening', () => {
  server.setBroadcast(true);
  const address = server.address();
  console.log(`listening ${address.address}:${address.port}`);
  loadMessages();
  sendAllHello();
  TIMER = timers.setInterval(timerCallback, 20*1000);
});

server.bind(PORT, ADDRESS); 


// Senders:
function sendAllHello() {
  var data = `ALL_HELLO ${getMsgCount()}`;
  data = Buffer.from(data);
  server.send(data, 0, data.length, PORT, BROADCAST_ADDR);
  console.log(`To all: ${data.toString()}`);
}

function sendAllMsg(msg_id, text) {
  var data = `ALL_MSG ${msg_id} ${text}`;
  data = Buffer.from(data);
  server.send(data, 0, data.length, PORT, BROADCAST_ADDR);
  console.log(`To all: ${data.toString()}`);
}

function sendGet(id, addr) {
  var data = `GET ${id}`;
  data = Buffer.from(data);
  server.send(data, 0, data.length, PORT, addr);
  console.log(`To ${addr}: ${data.toString()}`);
}

function sendMsg(id, text, addr) {
  var data = `MSG ${id} ${text}`;
  data = Buffer.from(data);
  server.send(data, 0, data.length, PORT, addr);
  console.log(`To ${addr}: ${data.toString()}`);
}


// Handlers:
function handleAllHello(msg, rinfo) {
  console.log('handleAllHello');
  var count = Number( msg.split(' ')[1] );
  var my_count = getMsgCount();
  if (count < my_count) {
    sendAllHello();
  } else if (count > my_count) {
      sendGet(my_count, rinfo.address);
  } else {
    console.log('I have the same messages count, do nothing.');
  }
}

function handleAllMsg(msg, rinfo) {
  console.log('handleAllMsg');
  var pos1 = msg.indexOf(' '); // first space
  var pos2 = msg.indexOf(' ', pos1+1); // second space
  var id = Number( msg.slice(pos1+1, pos2) );
  var text = msg.slice(pos2+1);
  var my_count = getMsgCount();
  if (id == my_count) {
    addMsg(id, text);
  } else if (id > my_count) {
    sendGet(getMsgCount(), rinfo.address);
  }
}

function handleGet(msg, rinfo) {
  console.log('handleGet');
  var id = Number( msg.split(' ')[1] );
  var text = MESSAGES[id];
  if (text !== undefined) {
    sendMsg(id, text, rinfo.address);
  }
}

function handleMsg(msg, rinfo) {
  console.log('handleMsg');
  var pos1 = msg.indexOf(' '); // first space
  var pos2 = msg.indexOf(' ', pos1+1); // second space
  var id = Number( msg.slice(pos1+1, pos2) );
  var text = msg.slice(pos2+1);
  var my_count = getMsgCount();
  if (id == my_count) {
    addMsg(id, text);
    sendGet(getMsgCount(), rinfo.address);
  } 
}

function handleIgnore(msg, rinfo) {
  console.log('handleIgnore: wrong opcode: ' + msg);
}


// Model:
function createMsg(text) {
  var id = getMsgCount();
  MESSAGES[id] = text;
  saveMessage(id);
  publish(id, text);
  return id;
}

function addMsg(id, text) {
  if (id != getMsgCount()) {
    console.log(`Error: Adding msg with id=${id}, but I have ${getMsgCount()} messages.`);
    return;
  }
  MESSAGES[id] = text;
  saveMessage(id);
  publish(id, text);
}

function getMsgCount() {
  return MESSAGES.length;
}

function saveMessage(msg_id) {
  try {
    fs.appendFileSync(STORAGE_FILE, MESSAGES[msg_id] + '\t');
  } catch (err) {
      console.log(err);
      return;
  }
  console.log(`Message with id=${msg_id} saved.`);
}

function loadMessages() {
  try {
    data = fs.readFileSync(STORAGE_FILE);
  } catch (err) {
    console.log(`Cannot find file ${STORAGE_FILE}`);
    return;
  }
  data = data.toString();
  if (data != "") {
    MESSAGES = data.split('\t');
    MESSAGES.pop(); // remove last emty entry
    console.log(`Loaded ${MESSAGES.length} messages.`);
  }
  for (i=0; i<getMsgCount(); i++) {
    publish(i, MESSAGES[i]);
  }
}

function timerCallback() {
  console.log('timerCallback');
  sendAllHello();
}


// Network:
function getMyIP(network='') {
  var os = require('os');
  var ifaces = os.networkInterfaces();
  var res = [];
  Object.keys(ifaces).forEach((ifname) => {
    ifaces[ifname].forEach((iface) => {
      if (!iface.internal && iface.family == 'IPv4')
        if (network == '' || iface.address.startsWith(network))
          res.push(iface.address);
    });
  });
  return res;
}

function broadcastAddr(addr) {
  var a = addr.split('.');
  return a[0] + '.' + a[1] + '.' + a[2] + '.255';
}


// DOM:
MSGS_CONTAINER = document.querySelector('#msgs_container');

document.querySelector('#sendRawBtn').addEventListener('click', (e) => {
  var text = document.querySelector('#msg_text').value;
  if (text.length > 0) {
    var data = Buffer.from(text);
    server.send(data, 0, data.length, PORT, BROADCAST_ADDR);
    console.log('To all: ' + text);
  }
});

document.querySelector('#sendBtn').addEventListener('click', (e) => {
  var elem = document.querySelector('#msg_text');
  var text = elem.value;
  if (text.length > 0) {
    msg_id = createMsg(text);
    sendAllMsg(msg_id, text);
    elem.value = '';
  }
});

function publish(id, text) {
  var div = document.createElement('div');
  div.className = "msg card";
  div.id = `msg_${id}`;
  div.innerHTML = `<div class="card-body">${text}</div>`;
  MSGS_CONTAINER.appendChild(div);
}

